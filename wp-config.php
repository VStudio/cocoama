<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'coco' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'MN|M_T;h0#MLGe5v1&|]f+Y~><)(x&2!.Sm 3Si}y}!R_O&,#r%WK)dQC ojD1=m' );
define( 'SECURE_AUTH_KEY',  'LPOeIhvSs&l5${07n@IM(?>XL:CJ7c?-`L34tfNO[:&j6l Ur<-k&o],ceZz~sl$' );
define( 'LOGGED_IN_KEY',    'sS}7v!^_ s<|i2>V^C<oA5eog7]WR_jh[d^Ar6hsLKWp_bH?d&izqSv@9?WoIGX9' );
define( 'NONCE_KEY',        'R|mLD-suAcVzMl;< cGTNh-X)P-e :O{m#ZzOV6gyDhOC4y6T>4weX.&9vx_4s.W' );
define( 'AUTH_SALT',        'rzz0zCe-$37zZFuOLtg`!vPO>Y[q,,ABG&7d=PAx)E]V|?C h[8Ezm`#J^Dr5D-u' );
define( 'SECURE_AUTH_SALT', ',&6b1ne~R{>_uR#:dm5,a/E2TS@`)24n9yk#><eR%pcNqE*UA%6qtcj!k75M35 O' );
define( 'LOGGED_IN_SALT',   '-nPz>F)9:bABz)67wx?5YgscD]e4>dEg*_U(A!vAPf-qZV*Q=R yR$Wqj-*@%MLB' );
define( 'NONCE_SALT',       '4U{6SNW,#N{wxm@ZsSSYo]%3GVc797;/uVg8$|F_Z{RpC6,6$T-kSK-7Fu>XqL1D' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'bd_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
